/*
 * Author: Phuong Duong - FPO Co.,Ltd
 * Email: phuongduong@fpo.vn
 * Created on 17:00:45 - 19/07/2021
 */
require("dotenv").config();
var axios = require('axios');
var utils = require('./utils');

exports.login = async (email, password) => {
    var config = {
        method: 'post',
        url: `${process.env.CMS_API_URL}/login`,
        headers: {
            'X-Requested-With': 'XMLHttpRequest',
            'Content-Type': 'application/json'
        },
        data: JSON.stringify({
            "email": email,
            "password": password
        })
    };

    const response = await axios(config)
    return response.data
}

exports.createBlog = async (token, blog) => {
    var config = {
        method: 'post',
        url: `${process.env.CMS_API_URL}/blog`,
        headers: {
            'X-Requested-With': 'XMLHttpRequest',
            'Authorization': `Bearer ${token}`,
            'Content-Type': 'application/json'
        },
        data: JSON.stringify({
            "name": blog.name,
            "description": blog.description,
            "content": blog.content,
            "thumbnail": blog.thumbnail,
            "cover": blog.cover,
            "category_id": null,
            "status": blog.status,
            "sort_order": blog.sort_order,
            "slug": utils.getSlug(blog.name),
            "meta_title": blog.name,
            "meta_description": blog.description,
            "meta_keywords": blog.tags.join(","),
            "tags": blog.tags
        })
    };

    const response = await axios(config)
    return response.data
}


