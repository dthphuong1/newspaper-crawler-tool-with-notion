# Crawler Tool based on Notion

Author: [Phuong Duong](https://phuongduong.fpo.vn/)

## How it works
1. Create a database on Notion which contain:
    - `Status` (checkbox) **required**
    - `Category` (select)
    - `Link` (url) **required**
2. The script will be read the Notion database and get the `Link`
3. Crawl post data with the `Link`
4. Add to your CMS database with CMS API
5. Update `post status` on Notion database

[![IMAGE ALT TEXT HERE](https://i9.ytimg.com/vi_webp/Lr2_l2f4A-g/mqdefault.webp?v=60f5555b&sqp=CNSq1YcG&rs=AOn4CLDZlxA8yWvXoKHhOQfXEqrs1k4dBg)](https://www.youtube.com/watch?v=Lr2_l2f4A-g)

## Prerequisite
- Notion API key: [https://developers.notion.com/](https://developers.notion.com/)
- Notion Page
- CMS API

## TODO
- [x] [Tuổi trẻ Online - TTO](https://tuoitre.vn)
- [x] [News Zing](https://zingnews.vn/)
- [x] [Thanh niên](https://thanhnien.vn/)
- [ ] [Tiền Phong](https://tienphong.vn/)
- [ ] [Sài Gòn giải phóng](https://www.sggp.org.vn/)
- [ ] [Vietnamnet](https://vietnamnet.vn/)
- [ ] [Pháp luật Online - PLO](https://plo.vn/)
## Usage
1. Clone the repository
```bash
git clone https://gitlab.com/dthphuong1/newspaper-crawler-tool-with-notion.git
cd newspaper-crawler-tool-with-notion
```

2. Prepare ENV file
```bash
cp .env.example .env
```
 After that open your .env file and fill in these fields

3. Run
```bash
npm install
node .
```

## Reference
[1] [Notion developer document](https://developers.notion.com/docs)

[2] [Getting Started with the Notion API and Its JavaScript SDK](https://www.sitepoint.com/notion-api-javascript-sdk/)

## Author
**Dương Trần Hà Phương (Mr.)** - CEO [Công ty TNHH FPO](https://fpo.vn)
- Email: [phuongduong@fpo.vm](mailto:phuongduong@fpo.vm)
- Website: [https://phuongduong.fpo.vn](https://phuongduong.fpo.vn)
- Gitlab: [@dthphuong1](https://gitlab.com/dthphuong1)
- Github: [@dthphuong](https://github.com/dthphuong)