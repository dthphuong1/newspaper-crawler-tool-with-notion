/*
 * Author: Phuong Duong - FPO Co.,Ltd
 * Email: phuongduong@fpo.vn
 * Created on 17:00:22 - 19/07/2021
 */
const DB = require('./database');
const API = require('./api');
const Parser = require('./newspaper_parser');
const _ = require("underscore");
const async = require("async");
const cheerio = require('cheerio');
const axios = require('axios');
const CronJob = require('cron').CronJob;

const ADMIN_USERNAME = process.env.ADMIN_USERNAME;
const ADMIN_PASSWORD = process.env.ADMIN_PASSWORD;

async.waterfall([
    (callback) => { // Authentication
        console.log(`[INFO]    START Authencication`);
        API.login(ADMIN_USERNAME, ADMIN_PASSWORD)
            .then((response) => {
                console.log(`[INFO]    TOKEN = ${response.token}`);
                console.log(`[SUCCESS] Authencicatted`);
                callback(null, response.token)
            })
            .catch((err) => {
                callback(err, null)
            })
    },
    (token, callback) => { // Get blog planner on Notion
        console.log(`[INFO]    START Get Blog Planner`);
        DB.getBlogs()
            .then((blogs) => {
                if (blogs.length > 0) {
                    // console.log(JSON.stringify(blogs, null, 4));
                    console.log(`[SUCCESS] Get ${blogs.length} blog(s)`);
                    callback(null, { blogs: blogs, token: token });
                } else {
                    callback(2, null);
                }
            })
            .catch((err) => {
                callback(err, null);
            })
    }
], (error, result) => {
    if (error) {
        if (error == 2) {
            console.log(`[ERROR]   No blogs are valid`);
        } else {
            console.log(`[ERROR]   ${error}`);
        }
    } else {
        // Crawl blog data
        console.log(`[INFO]    START Create blogs`);

        async.each(result.blogs, (blogSrc, callback) => {
            axios.get(blogSrc.url)
                .then((response) => {
                    if (response.status === 200) {
                        const html = response.data;

                        newsPaperData = Parser.parse(blogSrc.url, html);

                        let blogData = {
                            name: newsPaperData.title,
                            description: newsPaperData.desc.length > 255 ? newsPaperData.desc.match(/.{1,255}(\s|$)/g)[0] : newsPaperData.desc,
                            content: `${newsPaperData.content}<hr/><p style="text-align:right">Nguồn: ${newsPaperData.author} - ${newsPaperData.name}</p>`,
                            category_id: blogSrc.category,
                            thumbnail: newsPaperData.ogImage,
                            cover: newsPaperData.ogImage,
                            status: 1,
                            sort_order: 1,
                            tags: newsPaperData.tags
                        }

                        API.createBlog(result.token, blogData)
                            .then((response) => {
                                console.log(`[SUCCESS] Created blog with name: "${newsPaperData.title}"`);

                                // Update Notion posting status
                                DB.updateBlogStatus(blogSrc.id)
                                    .then((res) => {
                                        console.log(`[SUCCESS] Updated blog posting status`);
                                        // console.log(JSON.stringify(res, null, 4));

                                        callback();
                                    })
                                    .catch((err) => {
                                        console.log(`[ERROR]   ${err}`);
                                    })
                            })
                            .catch((err) => {
                                console.log(`[ERROR]   ${err}`);
                                callback(err);
                            })
                    }
                })
                .catch((err) => {
                    callback(err)
                });
        }, (error) => {
            if (error) {
                console.log(`[ERROR]   ${error}`);
            }
        })
    }
})

// var job = new CronJob({
//     cronTime: '0/30 * * * *', // Run every 30 minutes
//     onTick: () => {
//         console.log(`-------------------------------------------------------------------------------------------------`);
//         console.log(`[INFO]    START CRONJob at ${new Date()}`);

//         async.waterfall([
//             (callback) => { // Authentication
//                 console.log(`[INFO]    START Authencication`);
//                 API.login(ADMIN_USERNAME, ADMIN_PASSWORD)
//                     .then((response) => {
//                         console.log(`[INFO]    TOKEN = ${response.token}`);
//                         console.log(`[SUCCESS] Authencicatted`);
//                         callback(null, response.token)
//                     })
//                     .catch((err) => {
//                         callback(err, null)
//                     })
//             },
//             (token, callback) => { // Get blog planner on Notion
//                 console.log(`[INFO]    START Get Blog Planner`);
//                 DB.getBlogs()
//                     .then((blogs) => {
//                         if (blogs.length > 0) {
//                             // console.log(JSON.stringify(blogs, null, 4));
//                             console.log(`[SUCCESS] Get ${blogs.length} blog(s)`);
//                             callback(null, { blogs: blogs, token: token });
//                         } else {
//                             callback(2, null);
//                         }
//                     })
//                     .catch((err) => {
//                         callback(err, null);
//                     })
//             }
//         ], (error, result) => {
//             if (error) {
//                 if (error == 2) {
//                     console.log(`[ERROR]   No blogs are valid`);
//                 } else {
//                     console.log(`[ERROR]   ${error}`);
//                 }
//             } else {
//                 // Crawl blog data
//                 console.log(`[INFO]    START Create blogs`);

//                 async.each(result.blogs, (blogSrc, callback) => {
//                     axios.get(blogSrc.url)
//                         .then((response) => {
//                             if (response.status === 200) {
//                                 const html = response.data;

//                                 newsPaperData = Parser.parse(blogSrc.url, html);

//                                 let blogData = {
//                                     name: newsPaperData.title,
//                                     description: newsPaperData.desc,
//                                     content: `${newsPaperData.content}<hr/><p style="text-align:right">Nguồn: ${newsPaperData.author} - Tuổi trẻ Online</p>`,
//                                     category_id: blogSrc.category,
//                                     thumbnail: newsPaperData.ogImage,
//                                     cover: newsPaperData.ogImage,
//                                     status: 1,
//                                     sort_order: 1,
//                                     tags: newsPaperData.tags
//                                 }

//                                 API.createBlog(result.token, blogData)
//                                     .then((response) => {
//                                         console.log(`[SUCCESS] Created blog with name: "${title}"`);

//                                         // Update Notion posting status
//                                         DB.updateBlogStatus(blogSrc.id)
//                                             .then((res) => {
//                                                 console.log(`[SUCCESS] Updated blog posting status`);
//                                                 // console.log(JSON.stringify(res, null, 4));

//                                                 callback();
//                                             })
//                                             .catch((err) => {
//                                                 console.log(`[ERROR]   ${err}`);
//                                             })
//                                     })
//                                     .catch((err) => {
//                                         console.log(`[ERROR]   ${err}`);
//                                         callback(err);
//                                     })
//                             }
//                         })
//                         .catch((err) => {
//                             callback(err)
//                         });
//                 }, (error) => {
//                     if (error) {
//                         console.log(`[ERROR]   ${error}`);
//                     }
//                 })
//             }
//         })
//     },
//     onComplete: () => {
//         console.log(`[DONE]    Please visit this database to watch the result: https://www.notion.so/fpovn/c30072c008e1478096030a5ed94405d5?v=2c54093b6897495184d05975c87f4c9f`);
//     },
//     start: true,
//     timeZone: 'Asia/Ho_Chi_Minh'
// })