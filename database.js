/*
 * Author: Phuong Duong - FPO Co.,Ltd
 * Email: phuongduong@fpo.vn
 * Created on 17:00:30 - 19/07/2021
 */
require("dotenv").config();
const { Client } = require("@notionhq/client");
const _ = require("underscore");

// this line initializes the Notion Client using our key
const notion = new Client({ auth: process.env.NOTION_API_KEY });
const databaseId = process.env.NOTION_API_DATABASE;

exports.getBlogs = async () => {
    const response = await notion.databases.query({ database_id: databaseId });

    let result = []
    _.each(response.results, (item) => {
        if (!_.isUndefined(item.properties.Category)) {
            if (!_.isUndefined(item.properties.Status)) {
                if (!item.properties.Posted.checkbox && item.properties.Status.select.name == 'Approved') {
                    result.push({
                        id: item.id,
                        category: item.properties.Category.select.name,
                        url: item.properties.Link.url,
                        isPosted: item.properties.Posted
                    })
                }
            }
        }
    })

    // console.log(JSON.stringify(result, null, 4));

    return result;
};

exports.updateBlogStatus = async (blogId) => {
    const response = await notion.pages.update({
        page_id: blogId,
        properties: {
            "Posted": { "checkbox": true }
        }
    });

    // console.log(`LOG: ${JSON.stringify(response, null, 4)}`);
    return response
}


// temp

// axios.get('https://tienphong.vn/di-cho-giup-ba-con-vung-dich-voi-phi-ship-0-dong-post1356629.tpo')
//     .then((response) => {
//         if (response.status === 200) {
//             const html = response.data;
//             const $ = cheerio.load(html);

//             $('.cms-relate').remove();

//             let title = $('.cms-title').text().trim();
//             let desc = $('.cms-desc').text();
//             let content = $('.article-content').html();
//             let ogImage = $("meta[property='og:image']").attr("content");

//             console.log(`Title: ${title}`);
//             console.log(`Description: ${desc}`);
//             console.log(`og-image: ${ogImage}`);
//             console.log(`Content: ${content}`);

//         }
//     })
//     .catch((err) => {
//         throw new Error(err);
//     });
