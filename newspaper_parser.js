/*
 * Author: Phuong Duong - FPO Co.,Ltd
 * Email: phuongduong@fpo.vn
 * Created on 16:00:31 - 20/07/2021
 */

const cheerio = require('cheerio');
const _ = require('underscore');

function getNewspaperName(url = '') {
    if (url.indexOf('https://tuoitre.vn') != -1) {
        return 'tuoi-tre'
    }

    if (url.indexOf('https://zingnews.vn') != -1) {
        return 'zing-news'
    }

    if (url.indexOf('https://thanhnien.vn') != -1) {
        return 'thanh-nien'
    }

    if (url.indexOf('https://tienphong.vn/') != -1) {
        return 'tien-phong'
    }

    if (url.indexOf('https://www.sggp.org.vn') != -1) {
        return 'sggp'
    }

    if (url.indexOf('https://vietnamnet.vn') != -1) {
        return 'vn-net'
    }

    if (url.indexOf('https://plo.vn') != -1) {
        return 'phap-luat'
    }
}

exports.parse = (url, html) => {
    let newspaperName = getNewspaperName(url);
    let $ = cheerio.load(html);

    switch (newspaperName) {
        case 'tuoi-tre':
            $('div[type=RelatedOneNews]').remove();

            return {
                title: $('.article-title').text().trim(),
                desc: $('h2[class=sapo]').text(),
                content: $('#main-detail-body').html(),
                author: $('div[class=author]').html(),
                ogImage: $("meta[property='og:image']").attr("content"),
                tags: _.map($('li[class=tags-item] > a'), (tag) => { return tag.children[0].data }),
                name: 'Tuổi trẻ Online'
            }
            break;
        case 'zing-news':
            $('.inner-article').remove();

            return {
                title: $('h1[class=the-article-title]').text().trim(),
                desc: $('.the-article-summary').html(),
                content: $('.the-article-body').html(),
                author: $('div[class=the-article-credit] > p').text(),
                ogImage: $("meta[property='og:image']").attr("content"),
                tags: _.map($('p[class=the-article-tags] > span'), (tag) => { return tag.children[0].data }),
                name: 'Zing News'
            }
            break;
        case 'thanh-nien':
            $('.details__morenews').remove();

            return {
                title: $("meta[property='og:title']").attr("content"),
                desc: $("meta[property='og:description']").attr("content"),
                content: $('#contentAvatar').html() + $('#main_detail').html(),
                author: $("meta[property='dable:author']").attr("content"),
                ogImage: $("meta[property='og:image']").attr("content"),
                tags: _.map($("meta[property='article:tag']"), (tag) => { return tag.attribs.content }),
                name: 'Thanh niên'
            }
            break;
    }


}